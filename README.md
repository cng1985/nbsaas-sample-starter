# nbsaas-boot-starter

[![maven](https://img.shields.io/maven-central/v/com.nbsaas.boot/boot-nbsaas.svg)](http://mvnrepository.com/artifact/com.nbsaas.boot/nb-product-starter)
[![QQ](https://img.shields.io/badge/chat-on%20QQ-ff69b4.svg?style=flat-square)](//shang.qq.com/wpa/qunwpa?idkey=d1a308945e4b2ff8aeb1711c2c7914342dae15e9ce7041e94756ab355430dc78)
[![GitHub forks](https://img.shields.io/github/stars/nbsaas/boot-nbsaas.svg?style=social&logo=github&label=Stars)](https://github.com/nbsaas/boot-nbsaas)

#### 介绍

nbsaas-boot-starter是nbsaas-boot生态的业务初始化模块，包括接口定义，业务实现，前端业务，后端业务

## 编码规范

### 1.项目结构规范

```
{主工程}
{主工程}.apis
{主工程}.apis.xxx-api
{主工程}.apps
{主工程}.apps.xxx-app
{主工程}.code-generator
{主工程}.gates
{主工程}.gates.admins
{主工程}.gates.admins.xxx-admin
{主工程}.gates.fronts
{主工程}.gates.fronts.xxx-front
{主工程}.resources
{主工程}.resources.xxx-resource
```

## 使用

已经发布到maven中央仓库了

### 只使用resource模块

```
        <dependency>
            <groupId>com.nbsaas.boot</groupId>
            <artifactId>product-resource</artifactId>
            <version>1.0.9-2023</version>
        </dependency>
```

### 只使用admin模块(后台管理模块)

```
        <dependency>
            <groupId>com.nbsaas.boot</groupId>
            <artifactId>product-admin</artifactId>
            <version>1.0.9-2023</version>
        </dependency>
```

### 只使用front模块(前台业务模块)

```
        <dependency>
            <groupId>com.nbsaas.boot</groupId>
            <artifactId>product-front</artifactId>
            <version>1.0.9-2023</version>
        </dependency>
```