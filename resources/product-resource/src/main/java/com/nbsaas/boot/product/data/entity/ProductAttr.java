package com.nbsaas.boot.product.data.entity;

import com.nbsaas.boot.code.annotation.FormAnnotation;
import com.nbsaas.boot.jpa.data.entity.LongEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Comment;

@Comment("商品")
@Data
@FormAnnotation(title = "商品管理", model = "商品", showSelect = true)
@Entity
@Table(name = "nb_product_attr")
public class ProductAttr extends LongEntity {


}
