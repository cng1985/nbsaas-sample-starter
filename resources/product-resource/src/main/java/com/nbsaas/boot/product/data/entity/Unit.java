package com.nbsaas.boot.product.data.entity;

import com.nbsaas.boot.code.annotation.*;
import com.nbsaas.boot.code.annotation.data.Dict;
import com.nbsaas.boot.code.annotation.data.DictItem;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.rest.filter.Operator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Comment;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;


@Comment("商品单位")
@SimpleClass
@EqualsAndHashCode(callSuper = true)
@Data
@FormAnnotation(title = "单位", model = "单位", showSelect = true)
@Entity
@Table(name = "nb_sys_common_unit")
public class Unit extends AbstractEntity {


    @SearchItem(label = "单位名称", name = "name", operator = Operator.like)
    @FormField(title = "单位名称", grid = true, width = "200", required = true)
    @Column(nullable = false)
    @Comment("单位名称")
    private String name;

    @FormField(title = "单位符号", grid = true, col = 12)
    @Column(nullable = false)
    @Comment("单位符号")
    private String symbol;

    @Comment("转换率")
    private Double conversionRate; // 转换率

    @Column()
    @Comment("是否为基本单位")
    private Boolean baseUnit;

    @Dict(
            items = {
                    @DictItem(value = 1, label = "启用"),
                    @DictItem(value = 2, label = "禁用")
            }
    )
    private Integer status;

    @FormField(title = "描述", grid = true, type = InputType.textarea)
    @Column
    @Comment("描述")
    private String note;

    // Getters and Setters
}
