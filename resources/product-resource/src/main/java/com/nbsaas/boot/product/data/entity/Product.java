package com.nbsaas.boot.product.data.entity;


import com.nbsaas.boot.code.annotation.*;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.rest.filter.Operator;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Comment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Comment("商品")
@Data
@FormAnnotation(title = "商品管理", model = "商品", showSelect = true)
@Entity
@Table(name = "nb_product")
public class Product extends AbstractEntity {
    public static Product fromId(Long id) {
        Product result = new Product();
        result.setId(id);
        return result;
    }


    @Comment("商品编码")
    @FormField(title = "商品编码",  grid = false, col = 12,ignore = true)
    private String barCode;


    @Comment("商品主图")
    @FormField(title = "商品主图", grid = false, col = 24,type = InputType.image)
    private String logo;

    @Comment("商品名称")
    @SearchItem(label = "商品名称", name = "name")
    @FormField(title = "商品名称", grid = true, col = 12,required = true)
    private String name;

    @FormField(title = "价格", grid = true, col = 12, sort = true,required = true)
    private BigDecimal price;

    @FormField(title = "标准售价", grid = true, col = 12, sort = true,required = true)
    private BigDecimal standardPrice; // 标准售价

    @FormField(title = "批发价" )
    private BigDecimal wholesalePrice; // 批发价

    @FormField(title = "配送价" )
    private BigDecimal deliveryPrice; // 配送价

    @FormField(title = "销价1" )
    private BigDecimal salePrice1; // 销价1

    @FormField(title = "销价2" )
    private BigDecimal salePrice2; // 销价2

    @FormField(title = "销价3" )
    private BigDecimal salePrice3; // 销价3

    @FormField(title = "销价4" )
    private BigDecimal salePrice4; // 销价4

    @FormField(title = "最低售价" )
    private BigDecimal minPrice; // 最低售价

    @Comment("最高售价")
    @FormField(title = "最高售价" )
    private BigDecimal maxPrice; // 最高售价


    @FieldName
    @FieldConvert
    @FormField(title = "单位", grid = true, col = 12,type = InputType.select,option = "unit")
    @Comment("单位")
    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unit;

    @Comment("商品缩略图")
    @FormField(title = "商品缩略图", grid = false, col = 12,ignore = true)
    private String thumbnail;

    @SearchItem(label = "商品分类",name = "productCatalog",key = "productCatalog.id",classType = Long.class,operator = Operator.eq,show = false)
    @FormField(title = "商品分类", grid = false, col = 12,type = InputType.select,option = "productCatalog",ignore = true)
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCatalog productCatalog;


    @Comment("商品简介")
    @FormField(title = "商品简介", sortNum = "111", grid = false, col = 24, type = InputType.textarea)
    private String summary;

    @NoSimple
    @Comment("商品介绍")
    @FormField(title = "商品介绍", sortNum = "111", grid = false, col = 24, type = InputType.richText)
    private String note;

    @Comment("库存")
    @FormField(title = "库存", grid = true, col = 12,sort = true)
    private Long stockNum;

    @Comment("即时库存")
    @FormField(title = "即时库存", grid = true, col = 12,sort = true,ignore = true)
    private Long realStock;

    //更新库存日期
    private Date stockDate;



    @FormField(title = "餐盒费", col = 12,ignore = true)
    private BigDecimal mealFee;

    @FormField(title = "是否开启规格", col = 12,ignore = true)
    private Boolean skuEnable;

    @FormField(title = "是否必点", col = 12,ignore = true)
    private Boolean required;

    @FormField(title = "折扣",grid = true, col = 12,width = "11111",ignore = true)
    private BigDecimal discount;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private List<ProductSku> skus = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private List<ProductSpec> specs = new ArrayList<>();


}
