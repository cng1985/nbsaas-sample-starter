package com.nbsaas.boot.product.data.entity;

import com.nbsaas.boot.code.annotation.FormField;
import com.nbsaas.boot.code.annotation.InputType;
import com.nbsaas.boot.code.annotation.SearchItem;
import com.nbsaas.boot.jpa.data.entity.LongEntity;
import com.nbsaas.boot.rest.filter.Operator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Comment;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "nb_sys_common_unit_conversion")
@Comment("单位转换表")
public class UnitConversion extends LongEntity {

    @SearchItem(label = "来源单位", name = "fromUnit", key = "fromUnit.id", operator = Operator.eq, classType = Long.class)
    @FormField(title = "来源单位", grid = true, type = InputType.select, option = "fromUnit")
    @ManyToOne
    @Comment("来源单位")
    private Unit fromUnit;

    @SearchItem(label = "目标单位", name = "toUnit", key = "toUnit.id", operator = Operator.eq, classType = Long.class)
    @FormField(title = "目标单位", grid = true, type = InputType.select, option = "toUnit")
    @ManyToOne
    @Comment("目标单位")
    private Unit toUnit;

    @FormField(title = "转换率", grid = true, col = 12)
    @Column(nullable = false, precision = 18, scale = 6)
    @Comment("转换率")
    private BigDecimal conversionRate;

}
