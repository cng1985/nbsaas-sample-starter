package com.nbsaas.boot.product.data.entity;

import com.nbsaas.boot.code.annotation.CatalogClass;
import com.nbsaas.boot.code.annotation.FieldConvert;
import com.nbsaas.boot.code.annotation.FieldName;
import com.nbsaas.boot.code.annotation.FormAnnotation;
import com.nbsaas.boot.jpa.data.entity.CatalogEntity;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Comment;

import java.io.Serializable;
import java.util.List;

@CatalogClass
@Comment("商品分类")
@Data
@FormAnnotation(title = "商品分类管理", model = "商品分类")
@Entity
@Table(name = "nb_product_catalog")
public class ProductCatalog extends CatalogEntity {

    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCatalog parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<ProductCatalog> children;


    @Override
    public Serializable getParentId() {
        if (parent != null) {
            return parent.getId();
        }
        return null;
    }
}
